import socket
import threading
import argparse
import json


class Team:
    score = 0
    name = 0

    def __init__(self, score, name):
        self.score = score
        self.name = name


class Server:
    alive = True
    ip = '172.20.10.2'
    port = 1337
    file_port = 1338

    active_users = 10
    client_list = []
    teams = []

    buffer = 1024

    def __init__(self, ip, port, active_users):
        self.port = port
        self.active_users = active_users

    @staticmethod
    def __print_info__(message):
        print(f"[+] {message}")

    @staticmethod
    def __print_error_(error):
        print(f"[-] {error}")

    @staticmethod
    def __print_new_message__(message, name, address):
        print(f"[+] new massage {message} from {name}({address})")

    def __open_socket__(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(self.active_users)

        self.server_file_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_file_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_file_socket.bind((self.ip, self.file_port))
        self.server_file_socket.listen(self.active_users)

        self.__print_info__("Socket has been opened")

    def kill(self):
        self.alive = False
        for i in self.client_list:
            i.close()

    def relay_message(self):
        for i in self.client_list:
            try:
                message = json.dumps(
                    {
                        "teams": [self.teams[0].name, self.teams[1].name],
                        "score": [self.teams[0].score, self.teams[1].score]
                    }
                )
                i.send(message.encode())
                self.__print_info__(f"{message} to {i} has been sent")
            except OSError:
                pass

    def __init_teams__(self):
        for _ in range(0, 2):
            self.teams.append(Team(
                0, input("Team name: ")
            ))

    def serve_client(self):
        try:
            while self.alive:
                self.menu()
                self.relay_message()
        except UnicodeDecodeError:
            self.__print_error_("Unicode error")

    def menu(self):
        try:
            user_input = input("1. Change name\n2. Change score\n")
            if user_input == '1':
                self.change_name()
            if user_input == '2':
                self.change_score()
        except KeyboardInterrupt:
            print("exit")

    def change_name(self):
        try:
            user_input = input(f"Which team?\n1. {self.teams[0].name}\n2. {self.teams[1].name}\n")
            if user_input == '1':
                self.teams[0].name = input("Enter new name: ")
            if user_input == '2':
                self.teams[1].name = input("Enter new name: ")
        except KeyboardInterrupt:
            print("exit")

    def change_score(self):
        print(f"Current scores: 1. \n{self.teams[0].name} - {self.teams[0].score}\n 2. {self.teams[1].name} - {self.teams[1].score}")
        try:
            user_input = input("1. Enter manually\n2. Increase\n3. Decrease")
            user_input_team = input("Which team?")
            if user_input == '1':
                self.teams[int(user_input_team) - 1].score = int(input("Enter new score: "))
            if user_input == '2':
                self.teams[int(user_input_team) - 1].score += 1
            if user_input == '3':
                self.teams[int(user_input_team) - 1].score -= 1
        except ValueError:
            print("Integer error")
        except KeyboardInterrupt:
            print("exit")

    def close_connection(self, connection):
        for i in connection:
            i.close()
            self.client_list.remove(i)

    def serve(self):
        self.__open_socket__()
        self.__print_info__("Server has been started")

        self.__init_teams__()

        threading.Thread(target=self.serve_client).start()

        while self.alive:
            try:
                client_socket, client_address = self.server_socket.accept()
                self.client_list.append(client_socket)
                print("New connection")
                self.relay_message()
            except OSError:
                pass


def main():
    server = Server(ip='127.0.0.1', port=1337, active_users=128)
    server.serve()


if __name__ == '__main__':
    main()
