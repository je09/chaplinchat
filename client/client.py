from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QTabWidget, QWidget, QInputDialog, QLineEdit

import socket
import threading
import json
import sys

from ui.ui_main import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    server_port = 1337
    alive = True
    buffer = 1024
    server_ip = '172.20.10.2'

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.start()

    def __connect_socket__(self):
        try:
            self.socket = socket.socket()
            self.socket.connect((self.server_ip, self.server_port))
            self.__print_info__("Connected to server")
        except OSError:
            self.__print_info__("Can't connect! Check your server ip.")
            exit()

    @staticmethod
    def __print_info__(message):
        print(f"[+] {message}")

    def message_handler(self):
        while self.alive:
            try:
                message = json.loads(self.socket.recv(self.buffer).decode())
                self.__print_info__(f"New message {message}")
                self.ui.leftCount.setText(str(message["score"][0]))
                self.ui.rightCount.setText(str(message["score"][1]))
                self.ui.label.setText(message["teams"][0])
                self.ui.label_2.setText(message["teams"][1])
            except json.decoder.JSONDecodeError:
                self.__show_info__("JSON error")

    def start(self):
        self.__connect_socket__()
        threading.Thread(target=self.message_handler).start()
        self.__print_info__("You're connected now!")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = MainWindow()
    MainWindow.show()
    sys.exit(app.exec_())
